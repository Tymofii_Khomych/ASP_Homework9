﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using task1.Models;

namespace task1.Controllers
{
    public class HomeController : Controller
    {
        [UserTrackingFilter]
        public IActionResult Index()
        {
            return View();
        }
    }
}