﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    public class UserTrackingFilter : ActionFilterAttribute
    {
        private static readonly object _lock = new object();
        private const string _trackingFilePath = "AppData/user_tracking.txt";

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var ipAddress = context.HttpContext.Connection.RemoteIpAddress.ToString();
            var resultContext = await next();

            if (resultContext.Exception == null)
            {
                lock (_lock)
                {
                    var userTrackingInfo = $"{DateTime.UtcNow.ToString()} - IP: {ipAddress}{Environment.NewLine}";
                    File.AppendAllText(_trackingFilePath, userTrackingInfo, Encoding.UTF8);
                }
            }
        }
    }
}
