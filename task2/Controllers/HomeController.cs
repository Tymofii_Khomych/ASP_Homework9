﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using task2.Models;

namespace task2.Controllers
{
    public class HomeController : Controller
    {
        [ServiceFilter(typeof(ActionLoggingFilter))]
        public IActionResult Index()
        {
            return View();
        }

        [ServiceFilter(typeof(ActionLoggingFilter))]
        public IActionResult Privacy()
        {
            return View();
        }
    }
}