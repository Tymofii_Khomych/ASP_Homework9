﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.IO;
using System.Text;

namespace task2
{
    public class ActionLoggingFilter : IActionFilter
    {
        private static readonly object _lock = new object();
        private const string _logFilePath = "AppData/action_log.txt";

        public void OnActionExecuting(ActionExecutingContext context)
        {
            // Метод викликається перед виконанням методу дії
            var actionName = context.ActionDescriptor.DisplayName;
            var currentTime = DateTime.UtcNow.ToString();

            lock (_lock)
            {
                var logInfo = $"{currentTime} - Executing {actionName}{Environment.NewLine}";
                File.AppendAllText(_logFilePath, logInfo, Encoding.UTF8);
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // Метод викликається після виконання методу дії
            var actionName = context.ActionDescriptor.DisplayName;
            var currentTime = DateTime.UtcNow.ToString();

            lock (_lock)
            {
                var logInfo = $"{currentTime} - Executed {actionName}{Environment.NewLine}";
                File.AppendAllText(_logFilePath, logInfo, Encoding.UTF8);
            }
        }
    }

}
